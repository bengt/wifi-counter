#include <ESP8266WiFi.h>

// D0  does not work as input_pullup
// D4  lights led E12 module when pressed
// D8  does not work as input_pullup
// D9  does not work as input_pullup
// D10 does not work as input_pullup

const int PINS_LED[] = {
//  D0, D1, D2, D3, D4, D5, D6, D7, D8, D9
  D9, D8, D7, D6, D5, D4, D3, D2, D1, D0
};

const int PIN_BUTTON = D10;

const bool async = true;
const bool show_hidden= true;

int mode = 1;

void setup() {
    for (int pin_index = 0; pin_index < sizeof(PINS_LED); pin_index++) {
        pinMode(PINS_LED[pin_index], OUTPUT);
    }
    pinMode(PIN_BUTTON, INPUT_PULLUP);

    WiFi.mode(WIFI_STA);
    WiFi.disconnect();
    delay(100);
}

void loop() {
    if (digitalRead(PIN_BUTTON) == LOW) {
        mode = (mode + 1) % 3;
        for (int pin_index = 0; pin_index < sizeof(PINS_LED); pin_index++) {
            digitalWrite(PINS_LED[pin_index], LOW);
        }
    }

    int networksFound = WiFi.scanComplete();
    if (networksFound == -2) {
       WiFi.scanNetworks(async, show_hidden);
    }
    if (networksFound == -1) {
      // Scan in progress, so noop.
    }
    if (networksFound >= 0) {
      prinScanResult(networksFound);
    }
}

void prinScanResult(int networksFound) {
    if (mode == 0) {
        for (int pin_index = 0; pin_index < sizeof(PINS_LED); pin_index++) {
            if (bitRead(networksFound, pin_index) == 1) {
                digitalWrite(PINS_LED[pin_index], HIGH);
            } else {
                digitalWrite(PINS_LED[pin_index], LOW);
            }
        }
    }

    if (mode == 1) {
        for (int pin_index = 0; pin_index < sizeof(PINS_LED); pin_index++) {
            digitalWrite(PINS_LED[pin_index], LOW);
        }
        for (int pin_index = 0; pin_index < networksFound % 10; pin_index++) {
            digitalWrite(PINS_LED[pin_index], HIGH);
        }
    }

    WiFi.scanDelete();
}
